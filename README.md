This is a basic repo for spinning up a Kubernetes cluster to play with.

It was spun up on Ubuntu 16.04 using `--vm-driver=none`.

## Docs I used

[Minikube Quick Start](https://kubernetes.io/docs/setup/minikube/#quickstart)

[Install and Set Up kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)

[Tectonic PV docs](https://coreos.com/tectonic/docs/latest/admin/persistent-volumes.html)

[Kubernetes PVC docs](https://kubernetes.io/docs/concepts/storage/persistent-volumes/#persistentvolumeclaims)

[IBM hostPath PVs](https://www.ibm.com/support/knowledgecenter/en/SSBS6K_2.1.0/manage_cluster/create_hostpath.html)

[Kubernetes hostPath Volumes](https://kubernetes.io/docs/concepts/storage/volumes/#hostpath)

[Configure Pod Container](https://kubernetes.io/docs/tasks/configure-pod-container/)

[vSphere PVCs](https://vmware.github.io/vsphere-storage-for-kubernetes/documentation/persistent-vols-claims.html)

[GlusterFS docs](https://docs.gluster.org/en/latest/Troubleshooting/README/)